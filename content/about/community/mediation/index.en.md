---
title: "Mediation Process"
---

## Mediation Process
The mediation team is responsible for upholding the Code Of Coduct. Takes in reports for violations and acts on them. The members of the Medation Team are decided by each room that uses this Mediation Process, for the JoinJabber Collective it is the [Mediation Circle.](https://codeberg.org/joinjabber/collective/src/branch/joinjabber_mediation_team/Community%20Circles/Mediation%20Circle/Mediation%20Team.md#members)

In order to protect volunteers from abuse and burnout, we reserve the right to reject any report we believe to have been made in bad faith. Reports intended to silence legitimate criticism may be deleted without response.

## How to make reports
In case of harassment, abusive behavior, or if there's something else making you feel uncomfortable/unsafe/excluded, or have any other concerns, please contact a member of the mediation team that you are comfortable with.

Please note that people that manage the server have administrative access to the server. For the JoinJabber Collective these are members that are listed [here.](https://codeberg.org/joinjabber/collective/src/branch/joinjabber_mediation_team/Community%20Circles/Mediation%20Circle/Mediation%20Team.md#people-with-access-to-infrastructure) If a report is received that involves an sysadmin, all mediation team discussion and documentation will occur off the servers the room is hosted on. Sysadmins making a report and people reporting Sysadmins are encouraged to contact mediation members individually via private xmpp addresses.

If someone from the mediation/moderation teams is involved in a conflict, they would restrain themselves from participating in mediation activities regarding this conflict. The incident documentation will not be available to them, and they will excuse themselves from any conversations involving handling the incident.

Activities of the mediation team happens in a channel accessible only to its members. If necessary and with the consent of the person doing the report, an invitation can be sent to include other members of the mediation team.

## What to include in reports
Some general information that will helps us act on the report is listed below. If you don't remember all the details, we still encourage you to make a report.
- Your contact info (so we can get in touch with you if we need to follow up).

If you prefer to be anonymous, mention it to the mediation team member you are reporting to. They will not mention any names or any other contact information. This means you won't get any updates/followups later about the report.
- Date and time of the incident
- Whether the incident is ongoing
- Which online community and which part of the online community space it occurred in
- Description of the incident
- Identifying information of the reported person such as name, online username, handle, email address, xmpp address, or anything else
- A link to the conversation
- Any logs or screenshots of the conversation
- Additional circumstances surrounding the incident
- Other people involved in or witnesses to the incident and their contact information or description

## Mediation team Processes
If the incident is ongoing and needs to be immediately addressed, any member of the mediation team may take any action deem necessary for the safety of the person making the report and the safety of the community at their discretion.

A follow up will be sent to the person as soon as the mediation team meets that may include:
- An acknowledgment that the mediation team discussed the situation
- Whether or not the report was determined to be a violation of the Code of Conduct
- What short-term actions is the mediation team taking to make the person that did the report feel safe.

We will respect confidentiality requests for the purpose of protecting victims of abuse. At our discretion, we may publicly name a person about whom we've received harassment complaints, or privately warn third parties about them, if we believe that doing so will increase the safety of the members or the general public. We will not name harassment victims without their affirmative consent. 

Some incidents happen in one-on-one interactions, and even if the details are anonymized, the reported person may be able to guess who made the report. If you have concerns about retaliation or your personal safety, please note those in your report. We still encourage you to report, so that we can support you while keeping our community members safe. In some cases, we can compile several anonymized reports into a pattern of behavior, and take action on that pattern.

## Resolution
When we recieve a report we will contact the person(s) involved to have a conversation with them. We may revoke access to workshops, activities and physical or digital collaboration spaces if an individual's unacceptable behavior persists. We also may identify the participant as a harasser to other members or the general public.

A person from the mediation team will also follow up with the person that made the report to let them know:
- What action(s) were taken in response to the report. 
- If applicable what changes were made so that this does not happen again.

## Reading material for mediation team members and moderators
- https://conduct.gnome.org/committee-procedures/
- https://geekfeminism.fandom.com/wiki/Conference_anti-harassment/Policy
- https://geekfeminism.fandom.com/wiki/Conference_anti-harassment/Responding_to_reports
- https://policies.python.org/us.pycon.org/code-of-conduct/Enforcement-Procedures/

## References
- https://conduct.gnome.org/reporter-guide/
- https://lgbtq.technology/coc.html
