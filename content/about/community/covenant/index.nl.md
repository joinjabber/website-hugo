---
title: "Convenant voor servers"
---

_Als je op deze pagina terecht bent gekomen via een directe link op de website van een XMPP-server, betekent dit waarschijnlijk dat deze server ermee instemt zich te houden aan de onderstaande basisvoorwaarden._

### Basisvoorwaarden voor servers
_Alle XMPP-servers waarnaar wij linken, moeten instemmen met het volgende:_

- Actieve moderatie tegen discriminatie, intimidatie en ander onderdrukkend gedrag;
    - Mensen moeten het vertrouwen hebben dat ze toetreden tot een veilige ruimte, vrij van witte suprematie, antisemitisme en transfobie, die op andere platforms voorkomen. Daartoe moet een gedragscode worden opgesteld, die gebaseerd kan zijn op de [gedragscode van JoinJabber]({{< ref "about/community/codeofconduct" >}});
    - Dit vereist een betrouwbare manier om contact op te nemen met operators en een ontvangstbevestiging binnen 48 uur;
- Rate-limit en actieve controle van registraties om geautomatiseerde spam tegen te gaan;
    - Geautomatiseerde spam-blocklists en middelen om registraties te valideren worden ten zeerste aanbevolen, bijvoorbeeld een op uitnodiging gebaseerde systeem of out-of-band-registratie (d.w.z. registratie via een website).
- Regelmatig back-ups maken op een afzonderlijk opslagapparaat of -locatie;
    - Het is belangrijk dat mensen die de server gebruiken erop kunnen vertrouwen dat een struikelpartij over het netsnoer of een willekeurige bit flip niet al hun gegevens zal wissen. Het hebben van een back-upstrategie is een basisverantwoordelijkheid voor het beheren van een server voor anderen;
- Ten minste één andere persoon met noodtoegang tot de serverinfrastructuur hebben;
    - Verschillende omstandigheden kunnen verhinderen dat de beheerder van een XMPP-server technische noodgevallen kan beantwoorden. Daarom moet meer dan één persoon die mogelijkheid hebben;
- Beperk de toegang tot zowel de serversoftware als de hardware;
    - De dienst mag niet draaien op een gedeelde server waar een grote groep mensen ongecontroleerd toegang toe heeft;
    - Verificatie van toegang op afstand door middel van actuele, beste beveiligingspraktijken wordt verplicht gesteld. Momenteel zijn dit SSH-sleutelgebaseerde authenticatie en 2FA;
- Mensen die de server gebruiken hebben een manier om hun account te herstellen in geval van verlies van toegang;
    - Dit kan een gedeeld geheim zijn of een optioneel e-mailadres waarmee zij kunnen worden geverifieerd;
- Toezegging om ten minste 3 maanden van tevoren aan te kondigen dat de server wordt gesloten;
    - Soms worden servers afgesloten, dat is nu eenmaal de realiteit. Maar mensen die de server gebruiken moeten het vertrouwen hebben dat hun account niet van de ene dag op de andere zal verdwijnen, zodat zij de tijd hebben om hun contacten te informeren en een andere server te vinden;
    - Idealiter is er een manier om gemakkelijk gegevens te exporteren om naar een andere server te verhuizen.

### Basisvoorwaarden duurzaamheid
_Aanvullende criteria voor de exploitatie van een openbare XMPP-server:_

- Serverbeheerders hebben een bewezen staat van dienst voor het runnen van een openbare dienst;
    - Om mensen een soepele en betrouwbare start in het gedecentraliseerde Jabber-netwerk te geven is het niet raadzaam om gloednieuwe en onbewezen servers open te stellen voor het publiek;
    - Het wordt aanbevolen om nieuwe servers ten minste één jaar met beperkte toegang te laten draaien;
- Er bestaat een strategie voor duurzaamheid op langere termijn wat betreft financiering en beschikbaarheid van serverbeheerders; 
    - Dit wordt op transparante wijze meegedeeld aan de mensen die de dienst gebruiken en er bestaan manieren voor hen om bij te dragen;
    - Dit betekent niet dat de dienst commercieel moet zijn of door een professionele organisatie moet worden beheerd; vaak zijn kleine coöperaties zonder winstoogmerk of door vrijwilligers gerunde collectieven feitelijk duurzamer;
- Er zijn maatregelen om ongecontroleerde groei van de server te voorkomen;
    - Een gedecentraliseerd netwerk als geheel is duurzamer als mensen zich niet concentreren op een paar grote servers. Daarom zou een server met enkele duizenden leden moeten overwegen registraties te sluiten.

_Dit document is aan verandering onderhevig en zal zo nodig worden uitgebreid en aangepast. [Feedback wordt op prijs gesteld!](https://chat.joinjabber.org/#/guest?join=servers) ([xmpp](xmpp:servers@joinjabber.org?join))_ 

_Meer aanbevelingen voor het runnen van een (semi-)openbare XMPP-server zijn te vinden in [onze tutorials]({{< ref "tutorials/service/public" >}})._
