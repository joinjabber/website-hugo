---
title: "Server Covenant"
---

_If you reached this page directly linked from an XMPP server's website this likely means that this server agrees to be honor bound to the minimum criteria outlined below_

### Minimum service criteria
_All XMPP servers we link to will be required to agree to the following:_

- Active moderation against discrimination, harassment and other oppressive behavior;
    - People must have the confidence that they are joining a safe space, free from white supremacy, anti-semitism and transphobia present on other platforms. To this end a Code of Conduct should be in place, and may be based off JoinJabber's [Code of conduct](/about/community/codeofconduct/);
    - This includes a reliable way to contact operators and an acknowledgement usually within 48h;
- Rate-limit and actively monitor registrations to mitigate against automated spam;
    - Automated spam block-lists and means to validate registrations are highly recommended, these could include invitation based systems or out-of-band registration (i.e., registration through a website).
- Doing regular backups on a separate storage device or location;
    - It is important for people using the server to have the confidence that a trip over the power cable or a rogue bit flip will not erase all of their data. Having a backup strategy is a basic responsibility of operating a server for others;
- Having at least one other person with emergency access to the server infrastructure;
    - Various circumstances can prevent the a server operator of an XMPP server from answering technical emergencies. For this reason, more than one person must have that capability;
- Restrict access to both the server software and hardware;
    - The service should not be run on a shared server that a large group of people can access unchecked;
    - Remote access verification through up to date, security best practices, is enforced. Currently these are SSH key based authentication and 2FA;
- People using the server have a way to recover their account in case they lost access;
    - This could be a shared secret or an optional email-address that allows verifying them;
- Commitment to give notice at least 3 months in advance in case of shutting down;
    - Sometimes servers shut down, it is the cycle of life. But people using the server must have the confidence that their account will not disappear overnight, so that they have time to inform contacts and find another server;
    - Ideally a way to easily export data is provided to move to another server.

### Minimum sustainability criteria
_Additional criteria for running a public XMPP server:_

- Server operators have a proven track-record of running a public service;
    - To give people a smooth and reliable start into the decentralized Jabber network it is not advisable to open brand new and unproven servers to the public;
    - It is recommended to run new server with limited access for at least one year;
- A strategy exists to ensure longer term sustainability in regards to funding and availability of server maintainers;
    - This is transparently communicated to people using the service and ways for them to contribute exist;
    - This does not mean the service has to be commercial or run by a professional organization, often small non-profit coops or volunteer run collectives are actually more sustainable;
- Measures are in place to prevent unchecked growth of the server;
    - A decentralized network as a whole is more sustainable if people are not concentrated on a few large servers, therefore a server with several thousand members should consider closing registrations.

_This is a living document and will be extended and adapted as need be. [Feedback is appreciated!](https://chat.joinjabber.org/#/guest?join=servers) ([xmpp](xmpp:servers@joinjabber.org?join))_

_If you want to learn more best practices for running a (semi-)public XMPP server please have a look at our [tutorial on this](/tutorials/service/public/)._




