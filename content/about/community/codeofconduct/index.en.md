---
title: "Code of Conduct"
---

JoinJabber is a community dedicated to the concern of participants in the Jabber federated network. It promotes cultural diversity and international cooperation; we want to empower people to communicate without any form of oppression or barriers. See our [goals](https://joinjabber.org/about/goals/) for more information. 

This code of conduct applies to all our spaces, including xmpp channels and events/sprints, both online and off. We will take all good-faith reports of harassment seriously. Please report to the Mediation Team any problematic behavior you see regarding people in our chats or members of the JoinJabber collective. This includes behavior outside of JoinJabber spaces at any point in time.

We acknowledge that we come from different backgrounds and all have certain biases and privileges. Therefore, this Code of Conduct cannot account for all the ways that people might feel excluded, unsafe or uncomfortable. We commit to open dialogues, and as such this Code of Conduct is never finished and should change whenever needed.

Reports are to be addressed to the [mediation team](/about/community/mediation/). This list is not exhaustive and the mediation team may decide to take actions disregarding the list if necessary. A member of the community not doing anything explicitly against the Code of Conduct may also be requested to leave if they take too much time and energy away from the mediation team.

## Social Rules
We as a community pledge to make participation in our community a harassment-free experience for everyone, regardless of age, body size, visible or invisible disability, ethnicity, sex characteristics, gender identity and expression, level of experience, education, socio-economic status, nationality, personal appearance, race, caste, color, religion, or sexual identity and orientation.  If you're unsure if a word is derogatory, don't use it. This also includes repeated subtle and/or indirect discrimination.

JoinJabber prioritizes marginalized people's safety over privileged people's comfort. The mediation team reserves the right not to act on complaints regarding:
- "Reverse" -isms, including "reverse racism", "reverse sexism", and "cisphobia".
- Reasonable communication of boundaries, such as "leave me alone", "go away", or “I'm not discussing this with you”.
- Communicating in a "tone" you don't find congenial.
- Criticizing racist, sexist, cissexist, or otherwise oppressive behavior or assumptions.

Basic expectations for conduct are not covered by the "reverse-ism" clause and would be enforced irrespective of the demographics of those involved. For example, racial discrimination will not be tolerated, irrespective of the race of those involved. Nor would unwanted sexual attention be tolerated, whatever someone's gender or sexual orientation. Members of our community have the right to expect that participants in the project will uphold these standards.

### Unacceptable behavior within our spaces:
- Any form of stalking, harassing behavior, such as doxxing, evading blocks, repeated hostile messages against a person or a server (online or in person).
- Debating the rights and lived experiences of marginalized people in the community.
- The use of sexualized/violent language or imagery, and sexual attention or advances of any kind.
- Deliberate misgendering or use of "dead" or rejected names.
- Questioning or challenging someone's stated self-identity or chosen labels, even if they conflict with your own views.
- Deliberate outing of any aspect of a person's identity without their consent except as necessary to protect vulnerable people from intentional abuse.
- Child Sexual Abuse Material (CSAM), in all its manifestations (including animations or illustrations).
- Symbols and slogans associated with fascist regimes and/or fascist or white nationalist movements.
- Colonialism, imperialism in its all forms, nationalism (above all nationalism of nation states) and militarism.
- Promotion of capitalism or [meritocracy](https://postmeritocracy.org/).
- Publication of non-harassing private communication.
- Harassing photography or recording, including logging online activity for harassment purposes.
- Messages promoting or encouraging pseudoscience, conspiracies or misinformation.
- Arguments that seek to deny or cast doubt on historic or ongoing acts of genocide.
- Unwelcome physical contact. This includes touching a person without permission, including sensitive areas such as their hair, pregnant stomach, mobility device (wheelchair, scooter, etc) or tattoos. This also includes physically blocking or intimidating another person. Physical contact or simulated physical contact (such as emojis like “kiss”) without affirmative consent is not acceptable.
- Incitement of violence towards any individual, including encouraging a person to commit suicide or to engage in self-harm.

### Acceptable behavior within our spaces:
- Assuming good faith.
- Asking before assuming. For example what someone's preferred pronoun is, if they want to be touched, whether they know anything about a subject. If we are unsure, we ask for clarity. We also understand that not all questions are OK, or need answering.
- Respecting other beings, present or not, human or not.
- Be empathetic, by actively listening to others and not dominating discussions. We give each other the chance to improve and let each other step up into positions of responsibility. We make room for others. We are aware of each other's feelings, provide support while knowing when to step back. We ask to make sure that our actions are wanted. Leaving physical, emotional and conceptual room for other people.
- Encouragement to ask questions. No question is too obvious or should be known before asked.
- Considering the space and the ability of others.
- Giving and gracefully accepting constructive feedback. Accepting responsibility, apologizing to those affected by our mistakes, and learning from the experience.
- Focusing on what is best not just for us as individuals, but for the overall community. It sometimes means requesting dignity/respect/consideration for those who are not here.
- Caring for our physical and digital environment. We pay attention to the people, facilities, infrastructures and objects brought together and treat them with the necessary care.

## Culture
### The culture we want to encourage:
- Technology that tries to follow the [Permacomputing Principles](https://permacomputing.net/Principles/).
- We follow the [Social Rules](https://www.recurse.com/social-rules) of the Recurse collective.
- Consent: No unsolicited advice, respecting the autonomy of people using our server/app, respecting the privacy of other people in the network. See [Consentful Tech](https://www.consentfultech.io/).
- Plurality of views, experiences and backgrounds. This means that, especially as a white person, you might be called out on problematic behaviour. Do not confuse the discomfort this brings with being "unsafe". We expect you to be willing to examine your privilege, language and other habits while you are here, and work on a growing understanding of  [intersectional feminism](https://en.wikipedia.org/wiki/Intersectionality). We aim to be a place of learning, but we also expect you to do the work of understanding basic concepts yourself.
- Humility and empathy. People that know tech are not necessarily smarter or care more about subjects like privacy than other people.
- A spirit of [collaboration](https://codeberg.org/SocialCoding/Cooperative_Development_Guidelines) and consensus for working on projects. Each contributor participates in their own way according to their abilities, time and mental energy that they have. Decisions are taken with consideration to those affected by them, whether they have the ability/time/desire to participate in the project or not.
- Be open for self-reflection of our communities/people/figures/organizations. They can be wrong or do wrong things. We **should** speak up when we see them do wrong and it's okay to reform, remove the people that did/enable it or create new ones and denounce these that don't want to change.

### The culture we do not want to encourage:
- That all problems can be fixed with tech.
- That "freedom of speech" means everyone is obligated to listen to you.
- That the gender ratio in tech is a non-issue or is caused by women being "innately disinterested" in tech.
- That CoCs are oppression.
- That software accessibility is a lesser concern.
- That people should use FOSS/FLOSS/OSS even when it is not a good fit or they can't
- That developers are superior to people using the app/translators/designers, and the only ones whose opinions matter. We try to respect the dignity, experiences, and perspectives of those that are implicated by the work we do. It is very possible that those affected are not in the room with us.

## References
- https://hackersanddesigners.nl/code-of-conduct
- https://geekfeminism.fandom.com/wiki/Community_anti-harassment/Policy
- https://lgbtq.technology/coc.html
- https://www.thefeministclub.nl/coc/
- https://varia.zone/en/pages/code-of-conduct.html
- https://constantvzw.org/w/?u=http://media.constantvzw.org/wefts/123/
