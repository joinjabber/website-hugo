---
title: "Collective goals"
weight: 2
---

When we founded our collective, we gave ourselves a set of goals. On this page, you will find the updated list of our goals, refined over time. These goals were debated in English language during our meetings, and contributions are welcome.

- JoinJabber is a community dedicated to the concerns of end-users of the Jabber federated network
- JoinJabber promotes cultural diversity and international cooperation; we want to empower people to communicate without any form of oppression or barriers; any language is welcome in the community, although each language has its own channels (forums, chats…).
- Communication and decision-making within the JoinJabber collective takes place in English, and we encourage speakers of other languages to express their concerns via translations and/or delegates of their choice. The scheduled agenda for a meeting is published and translated in advance on a best-effort basis, and so are meeting minutes.
- JoinJabber is a free association, which promotes empowerment of individuals and communities; every person is free to express desires and concerns during our meetings, and we have no formal membership
- JoinJabber opposes toxic and oppressive behavior such as stalking, harrassment, and spamming; we will ban people harming our users and our community
- JoinJabber is not involved in any form of commercial activity: we may raise donations for JoinJabber infrastructure, advertise existing fundraisers for solutions we recommend (clients, servers, and server operators), or take part in fundraisers for new features of interest to end users
- JoinJabber promotes decentralized governance: we have no authority over any project, though we may choose to recommend or avoid recommending projects depending on further-established criteria
- JoinJabber promotes inter-project cooperation and would like to encourage adoption of modern standards focused on the needs of end-users
- JoinJabber promotes decentralized Internet infrastructure: we do not want to become a big central server, but rather promote existing user-friendly servers and the selfhosting of new servers
- JoinJabber provides services and infrastructures for collective projects around the Jabber ecosystem, but does not provide services to end-users (such as Jabber accounts)
- JoinJabber provides support channels (instant chat and long-lived discussions) to help users of Jabber clients/servers; we do not want to replace existing support channels for specific projects, but would like to help lower their burden with simple matters; support is provided by volunteers on a best-effort basis
- JoinJabber promotes privacy as a fundamental human right, and as a pillar of popular power and autonomy; we take part in evaluating privacy of the Jabber ecosystem, and proposing recommendations for implementers, for server operators, and for end-users; the use of a nickname is recommended (though not mandatory) to participate in the JoinJabber collective
- JoinJabber studies user experience across the Jabber ecosystem, and collects end-user feedback; this feedback, aggregated from testimonies and field-studies (for example during install parties) can be used to propose recommendations for implementers and server operators
- JoinJabber promotes interoperability between Jabber and other free, decentralized networks; we can only stand up to the Internet giants if we are able to communicate across free solutions (such as IRC, Matrix, Fediverse..)
- JoinJabber promotes accessibility to all services, regardless of physiology (disabilities) and resource limitations (slow Internet access, low-end hardware, or lack of money).
- JoinJabber encourages reproducible builds for more reliable and trustworthy software. JoinJabber may provide software repositories to encourage quicker, secure updates for existing client/server software across operating systems.
