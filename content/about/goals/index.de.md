---
title: "Ziele unseres Kollektivs"
weight: 2
---

Leider haben wir diese Seite noch nicht übersetzt ([Hilfe dabei gerne gesehen](https://codeberg.org/joinjabber/website#helping-with-translations)). Das [englische Original findest du hier](/about/goals/).
