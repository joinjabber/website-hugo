---
title: "JoinJabber"
---

{{< block "grid-2" >}}
{{< column >}}

# Welkom bij de JoinJabber-gemeenschap!

_Een inclusieve plek op het Jabber-netwerk_

{{< tip "warning" >}}
Met Jabber kun je [veilig](docs/faqs/user/#faq-users-encryption) chatten of bellen met je vrienden en familie. Ook kun je deelnemen aan openbare groepschats met mensen die je interesses delen, zonder persoonlijke gegevens prijs te geven.
{{< /tip >}}

{{< tip >}}
Jabber, [ook bekend als **XMPP**](docs/faqs/user/#faq-users-xmppjabber), is een _open standaard_ voor online communicatie. Dit betekent dat het Jabber-netwerk van ons allemaal is, niet van een enkele organisatie.

Jabber is [federatief](docs/faqs/user/#faq-users-federation), net als e-mail of Mastodon: vele servers zijn met elkaar verbonden om samen het Jabber-netwerk te creëren.
{{< /tip >}}

{{< tip "warning" >}}
Op deze website helpen we je in twee eenvoudige stappen gebruik te maken van Jabber.

Verder nodigen we je uit om je aan te sluiten bij [ons project](about/goals/), waarmee we de mogelijkheden van het Jabber-platform verder willen uitbreiden.
{{< /tip >}}

{{< button "docs/" "Laten we beginnen!" >}}{{< button "https://chat.joinjabber.org/" "Chat met ons" >}}
{{< /column >}}

{{< column >}}
![](/images/undraw_texting.svg)
{{< /column >}}
{{< /block >}}
