---
title: "Tutorials"
weight: 1
---

- **[Gateways](/tutorials/gateways/)**: connect to other networks from your Jabber/XMPP client
- **[Integrations](/tutorials/integration/)**: How to link XMPP to other services
- **[Self-hosting](/tutorials/self-hosted/)**: How to run your own XMPP server
- **[XMPP services](/tutorials/service/)**: How to provide additional public services
