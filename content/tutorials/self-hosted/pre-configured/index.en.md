---
title: "Pre-configured"
description: "Pre-configured software for self-hosting"
weight: 1
---

_This document is still a work in progress!_

It is of course possible to run your own XMPP server. Here you will find some options for doing this with minimal effort:

## Snikket

[<img alt="Get started" src="/images/servers/snikket.svg" style="max-height:60px;height:100%">](https://snikket.org/service/quickstart/)

Snikket is a project started by one of the [Prosody](https://prosody.im) developers to provide a unified app experience including user friendly server hosting. Learn more about [their goals here](https://snikket.org/about/goals/).

## Yunohost

[<img alt="Get started" src="/images/servers/yunohost.svg" style="max-height:100px;height:100%">](https://yunohost.org)

YunoHost is an operating system aiming for the simplest administration of a server, in order to democratize self-hosting. You can easily install an [Prosody XMPP server](https://apps.yunohost.org/app/prosody) alongside many other software for your server.

## Uberspace

[<img alt="Get started" src="/images/servers/uberspace.svg" style="max-height:100px;height:100%">](https://uberspace.de/en/)

If you prefer something in-between a managed service and running your own server or VPS, Uberspace has you covered. They provide a [detailed tutorial how to run your own Prosody XMPP service](https://lab.uberspace.de/guide_prosody/) on their shared server.

**Others?**

This is obviously not an exhaustive list and if you have a really good recommendation please contact us here: {{< chatlink "servers" >}}servers@joinjabber.org (web chat){{< /chatlink >}}





