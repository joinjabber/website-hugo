---
title: "Self-hosting"
---

We recommend using [Prosody](https://prosody.im/doc) or [Ejabberd](https://docs.ejabberd.im/) for hosting your own XMPP server. But there are pre-configured options make it a bit easier to get started.

1. **[Pre-configured](/tutorials/self-hosted/pre-configured)**
2. **[Ejabberd](/tutorials/self-hosted/ejabberd)**
3. **[Prosody](/tutorials/self-hosted/prosody)**

If you plan to allow others to use your XMPP server, please consider our [Server Covenant](/about/community/covenant) and maybe have a look at our [tutorial for running a (semi-)public service](/tutorials/service/public).

Homebrew Server club is also a nice place to [learn more](https://homebrewserver.club/category/instant-messaging.html).
