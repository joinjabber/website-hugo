---
title: "Prosody"
description: "Lightweight XMPP server in lua"
weight: 3
---

_This document is still a work in progress!_

[Prosody](https://prosody.im/) is a lightweight XMPP server, that can be described as a highly configurable tool-box to customize your own chat server. However, this also means that out of the box it is pretty barebones and requires a lot of configuration. You can find the [official documentation here](https://prosody.im/doc), but if you are looking for a simpler option then please have a look at [Snikket](https://snikket.org/), which is based on Prosody.

**Support chat**

This is obviously not an exhaustive list and if you have a really good recommendation please contact us here: {{< chatlink "servers" >}}servers@joinjabber.org (web chat){{< /chatlink >}}





