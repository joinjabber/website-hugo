---
title: "Gateways"
---

In the Jabber/XMPP ecosystem, gateways (transports) are the means to connect to different protocols via your client.

1. **[IRC](/tutorials/gateways/irc/)**
2. **[Matrix](/tutorials/gateways/matrix/)**
3. **[Slidge](/tutorials/gateways/slidge/)**

You can also join our gateways and bridges group chat here: {{< chatlink "bridging" >}}bridging@joinjabber.org (web chat){{< /chatlink >}}

