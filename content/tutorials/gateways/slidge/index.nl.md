---
title: "Slidge Gateways"
---

[Slidge](https://slidge.im/) is een multi-netwerk [puppeteering gateway]({{< ref "docs/faqs/gateways#terminologie" >}}) in ontwikkeling. Dit betekent dat in tegenstelling tot andere opties nog steeds een account op het betreffende netwerk nodig is.

Momenteel ondersteunt het verbindingen met:

- [Discord](https://slidge.im/slidcord/) ([source-code](https://git.sr.ht/~nicoco/slidcord))
- [Facebook Messenger](https://slidge.im/messlidger/) ([source-code](https://git.sr.ht/~nicoco/messlidger))
- [Mattermost](https://slidge.im/matteridge/) ([source-code](https://git.sr.ht/~nicoco/matteridge))
- [Matrix](https://slidge.im/matridge/) ([source-code](https://git.sr.ht/~nicoco/matridge))
- [Signal](https://slidge.im/slidgnal/) ([source-code](https://git.sr.ht/~nicoco/slidgnal))
- [Skype](https://slidge.im/skidge/) ([source-code](https://git.sr.ht/~nicoco/skidge))
- [Steam Chat](https://slidge.im/sleamdge/) ([source-code](https://git.sr.ht/~nicoco/sleamdge))
- [Telegram](https://slidge.im/slidgram/) ([source-code](https://git.sr.ht/~nicoco/slidgram))
- [WhatsApp](https://slidge.im/slidge-whatsapp/) ([source-code](https://git.sr.ht/~nicoco/slidge-whatsapp))

### Bekende openbare instanties

Momenteel geen bekende openbare instanties van Slidge. Omdat de gebruikersgegevens van het legacy-netwerk op de server moeten worden opgeslagen, is het onwaarschijnlijk dat er echte openbare instanties worden opgezet. Je XMPP-serverbeheerder (en dat zou je zelf kunnen zijn) zou het echter kunnen aanbieden als een vertrouwde dienst.

### Schrijf je eigen plug-in voor Slidge

Slidge is ontworpen als een raamwerk voor gateways en kan gemakkelijk [uitgebreid  worden](https://slidge.im/core/dev/tutorial.html) als er een bibliotheek (idealiter in Python), een CLI-client of een web-API voor het legacy-netwerk bestaat.
