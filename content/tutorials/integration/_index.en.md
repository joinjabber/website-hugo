---
title: "Integrations"
---

Here you can find some tutorials on XMPP integrations:

- **[Akkoma/Pleroma](/tutorials/integration/akkoma/)**
- **[Forgejo](/tutorials/integration/forgejo/)**
- **[Friendica](/tutorials/integration/friendica/)**
- **[LDAP](/tutorials/integration/ldap/)**
- **[Lemmy](/tutorials/integration/lemmy/)**
- **[Mastodon](/tutorials/integration/mastodon/)**
- **[Nextcloud](/tutorials/integration/nextcloud/)**
- **[Sharkey](/tutorials/integration/sharkey/)**
