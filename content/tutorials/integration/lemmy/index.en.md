---
title: "Lemmy"
---

How to easily integrate a XMPP server with a [Lemmy](https://join-lemmy.org/) link aggregator:

### Ejabberd external auth

Currently the only way to link an [Ejabberd server](https://www.ejabberd.im/) to a Lemmy instance on the same server is to use an [external auth script](https://f-hub.org/Solarpunk/ejabberd-auth-lemmy) to directly interact with the Lemmy Postgresql database. For this you first need to expose the internal database port to the host network if you are using the standard Docker based deployment of Lemmy.

### Prosody

We are still looking into the best options to directly link Prosody to Lemmy, but it would be probably possible to adapt the [auth middleware for Sharkey](/tutorials/integration/sharkey) to connect to a Lemmy Postgres database instead. Please let us know if you get this to work.
