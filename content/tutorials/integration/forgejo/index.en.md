---
title: "Forgejo"
---

How to easily integrate a XMPP server with a [Forgejo](https://forgejo.org/) git forge:

*Note: This likely also works with Gitea, but is currently untested.*

### Ejabberd external auth

Currently the only way to link an [Ejabberd server](https://www.ejabberd.im/) to a Forgejo installation on the same server is to use an [external auth script](https://f-hub.org/Meta/ejabberd-auth-forgejo) to directly interact with the Forgejo Postgresql database. For this you first need to expose the internal database port to the host network if you are using the standard Docker based deployment of Lemmy. Please also note that this authscript requires switching to the non-default bcrypt hashing algorithm.

### Prosody

We are still looking into the best options to directly link Prosody to Forgejo, but it would be probably possible to adapt the [auth middleware for Sharkey](/tutorials/integration/sharkey) to connect to a Forgejo Postgres database instead. Please let us know if you get this to work.
