---
title: "Toegang via Tor"
---

### Hoe kan ik mijn server federeren met onion-servers? {#faq-op-onions}

Eerst moet je ervoor zorgen dat [Tor](https://www.torproject.org/) op je server geïnstalleerd is. Vervolgens zijn er twee manieren om het aan te pakken. Prosody server heeft een uitstekende Tor/onion-integratie met [mod_onions](https://modules.prosody.im/mod_onions.html).

Als je deze module niet wilt gebruiken, of als je Prosody niet gebruikt, dan kun je overwegen om Tor te configureren als [een transparante proxy](https://gitlab.torproject.org/legacy/trac/-/wikis/doc/TransparentProxy). Tor zal antwoorden op DNS-verzoeken over onion-diensten (bijvoorbeeld op poort 5352), en een record aanmaken voor een lokaal adres (bijvoorbeeld in het bereik "127.192.0.0/10"), dat iptables zal doorsturen via Tor's SOCKS5-proxy (bijvoorbeeld, "127.0.0.1:9040").

**Stap 1:** Toevoegen aan Tor-configuratie (meestal "/etc/tor/torrc"):
```
VirtualAddrNetworkIPv4 127.192.0.0/10
VirtualAddrNetworkIPv6 [FE80::]/10
AutomapHostsOnResolve 1
TransPort 127.0.0.1:9040
TransPort [::1]:9040
DNSPort 5352
```
**Stap 2:** Toevoegen aan iptables-configuratie (meestal "/etc/iptables/iptables.rules"):
```
-A OUTPUT -d 127.192.0.0/10 -p tcp -j REDIRECT --to-ports 9040
```
**Stap 3**: Toevoegen aan dnsmasq of andere resolver-configuratie (meestal "/etc/dnsmasq.conf")

Als laatste configureer je de DNS-resolver om Tor's transparante onion-resolver te gebruiken als bron van autoriteit voor het ".onion"-domein. Met dnsmasq kun je bijvoorbeeld eenvoudig aan "/etc/dnsmasq.conf" toevoegen:
```
server=/onion/127.0.0.1#5352
```
