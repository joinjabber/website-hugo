---
title: "Public servers"
---
_This document is still a work in progress!_

So you got your personal XMPP server running for some time and everything seems fine? Maybe some friends already joined you on your server and you made new ones on the [many public Jabber channels](https://search.jabber.network). At some point you might have asked what you could do to contribute further to the Jabber community and someone pointed you to this guide? Well, you have come to the right place!

### Running a (semi-)public XMPP server
Jabber as a decentralized network lives from everyone contributing in various ways according to means and capabilities. Not everyone can or wants to run their own XMPP server. So it is great that some people step up and volunteer to run a service for others. This doesn't mean that it has to be public with registrations open to everyone of course. But even on a small scale, opening one's service comes with certain responsibilities, both towards the users of your server and by being a part of the Jabber federation.

### Is it advisable to have open registrations?
Opening a server to public registrations through a website or within the application (so called In-Band-Registration IBR) can be a nice way to allow anyone to sign up and join the Jabber network. However, it comes with a serious risk of the server being abused for spam or users creating fake accounts to harass other people. There are some technical means to limited the possible extend of such abuse (see below), but we would like you to encourage to  think of other means to manage registrations like user-invites or [linking the registration to another service](/tutorials/integration/) where only trusted users can sign up. While this is a small entry barrier for new users, this will help keep the network healthy and growth organic.

### Service description guidelines
_Your server's description should be in a presentable state:_
- Provide a short description of what makes your server different from others. Is it hosted by a specific organization? Is it operated from a specific country or region? Stick to one or two sentences. Attention is a limited resource! If your server's target audience speaks a different language than English, write the description in that language
- The jurisdiction the server falls under is especially important for legal requirements like the [EU's GDPR](https://en.wikipedia.org/wiki/General_Data_Protection_Regulation);
- Since when this service has been in operation (interesting for users to know)
- Transparent outline of the server's retention policies
  - How long are messages stored in the MAM archive (& how long are backups archived?)
  - How long are image/file uploads retained
- Transparent outline of the server's upload limits etc.
  - Maximum individual file-size & overall storage limit
  - Daily/Weekly/Monthly upload limits if they exist
- Transparent server rules: server rules are an integral part of on-boarding. They must be kept short and to the point (you can elaborate in an extended server description). As a rule they should fit on a typical phone screen without having to scroll through. You want the visitor to be able to read them quickly
- Clear outline of sign-up requirements and processes
- Contact information (XMPP & e-mail)
  - You should have an XMPP contact account (it does not have to be your personal account). It would be good to also have a valid and reachable e-mail address for more serious abuse reports or legal inquiries
- Usage statistics: approximate total number of users, typically daily active users etc.
- Easy to find link for account recovery, data-export, management and deletion (see below).

### GDPR requirements (not legal advice)
_If you have users from the European Union there are certain legal requirements that you need to fulfill._
- Have a Terms of Service (ToS) document and a Privacy Policy
  - [A template can be found here](https://wiki.xmpp.org/web/GDPR/ToS_Template)
- Optional data export: [more details here](https://docs.modernxmpp.org/projects/portability/)
- Easy option for user account deletion
  - Can be done via IBR, even when public registration is disabled
- 3rd party personal data processors need to be clearly identified in the Privacy Policy
  - For example an external service provider that handles outgoing registration emails

### Anti-spam measures

- Rate limits for sending messages
- Rate limits for registrations and active monitoring and or manual approval of sign-ups
- [Real time block list subscription](https://xmppbl.org):
  - Prosody: via [mod_muc_rtbl](https://modules.prosody.im/mod_muc_rtbl.html)
  - Ejabberd: Supported since version 23.04
  - OpenFire: [RTBL plugin](https://www.igniterealtime.org/projects/openfire/plugin-archive.jsp?plugin=mucrtbl)
- Spam reporting and report forwarding, see [tutorial for Prosody here](https://xmppbl.org/reports)

### Other useful features for server operator

- Announce messages to all people using the server "mod_announce" for both Prosody and Ejabberd
- Advertise contact information: server_contact_info for Prosody and mod_disco for Ejabberd
- `mod_watchuntrusted` (Prosody only) to warn admins when an s2s connection fails due for encryption or trust reasons
- `mod_default_bookmarks` to automatically invite a person that just signed up to a support room
- Push notifications for mobile devices: `mod_cloud_notify` in Prosody and `mod_push` for Ejabberd
- DNSSEC setup, [CertWatch can help](https://certwatch.xmpp.net/)

### JoinJabber.org Server Convenant

- If you run a public service please consider signing up to our [Server Convenant](/about/community/covenant)
  - While we have only limited means to verify compliance of course, we hope that server admins will be honour bound to adhere to these minimum service and sustainability criteria
- Server admins are also welcome to link to our [FAQ](/docs/faqs) and [recommended apps list](/docs/apps) to avoid duplicating general information about Jabber/XMPP





