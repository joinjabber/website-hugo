---
title: "Unified Push"
---

[UnifiedPush](https://unifiedpush.org/) (UP) is an alternative for mobile push-notifications on Android that does not depend on the centralized Google FCM infrastructure for delivering notifications to your smart-phone. Especially for apps installed via the [F-Droid catalogue](https://f-droid.org/) this can be very useful and they recently wrote a [nice explanation about it on their blog](https://f-droid.org/en/2022/12/18/unifiedpush.html).

### Apps using UP

While UnifiedPush is a relatively new technology, there has been already quite some up-take especially with Fediverse (ActivityPub) apps and a a few Matrix clients. The UnifiedPush website [lists a few here](https://unifiedpush.org/users/apps/). Contrary to these apps that use UP to send notifications, XMPP apps are more useful as so called "UP distributors" that act as the primary receivers of UP notifications. This is because XMPP is designed exactly for such type of purposes and even the Google FCM notification service uses XMPP under the hood.

### Conversations as a UP distributor

The distributor is the application you install on your device to get notifications. [The Conversations app]({{< ref "docs/apps/android#conversations-android" >}}) has the ability to deliver UnifiedPush notifications using an existing XMPP account, on any server. This requires converting push notifications to XMPP messages first, which can be for example done by the service hosted at `up.conversations.im`.

To enable receiving UnifiedPush you just need to:

1. Open Conversations Settings
2. Scroll down to UnifiedPush Distributor
3. Select the XMPP Account you want to receive notifications through
4. You’re ready to use UnifiedPush!

Afterwards you can configure other non-XMPP apps on your mobile to use this distributor.

### Self-hosting

A service like `up.conversations.im` can also be self-hosted. It is called a rewrite proxy in UnifiedPush parlance. In addition to a regular XMPP server, you need a UnifiedPush to XMPP Rewrite Proxy to turn UnifiedPush messages into XMPP messages. There are currently two options for rewrite proxies:

- [up is a proxy written in Java](https://codeberg.org/iNPUTmice/up) by the developer of Conversations. It works with any external XMPP server.
- [mod_unified_push](https://modules.prosody.im/mod_unified_push) is an extension that turns [Prosody](https://prosody.im/) into a complete push server.
