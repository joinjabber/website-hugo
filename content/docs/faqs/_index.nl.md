---
title: "FAQ's"
description: "Veelgestelde vragen"
weight: 4
---

Hier vind je meer gedetailleerde informatie over het platform:

- **[FAQ voor gebruikers](user/)**: common questions for typical users
- **[FAQ voor gevorderden](advanced/)**: Meer gedetailleerde vragen over Jabber/XMPP
- **[FAQ voor veiligheid](security/)**: Specifieke vragen over veiligheid/privacy
- **[FAQ voor gateways](service/)**: vragen over bridges naar andere diensten
- **[FAQ voor beheerders](service/)**: Vragen over serverbeheer

