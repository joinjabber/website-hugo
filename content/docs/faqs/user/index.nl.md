---
title: "FAQ voor gebruikers"
description: "Vragen voor/van eindgebruikers"
weight: 1
---

### Wat is XMPP/Jabber? {#faq-users-what}

[XMPP](https://xmpp.org/) is een open standaard (protocol) voor communicatie dat vooral gebruikt wordt voor real-time instant messaging en groepschats. Als je via bijvoorbeeld WhatsApp, Fortnite, of Zoom communiceert, dan gebruik je XMPP al. Maar dat zijn gesloten netwerken, waar al je gegevens eigendom zijn van één bedrijf en de gebruikers van de ene dienst niet kunnen communiceren met de gebruikers van een andere dienst. Het XMPP-protocol kan worden gebruikt om gebruikers te laten communiceren tussen verschillende dienstverleners (net als e-mail of Mastodon); deze open federatie wordt ook wel de _Jabber-federatie_ genoemd. Jabber/XMPP is ook een vriendelijke gemeenschap van gebruikers en ontwikkelaars die waarde hechten aan privacy en vrijheid van communicatie. De meesten van ons zijn onbetaalde vrijwilligers en iedereen is welkom om deel te nemen aan dit project.

### Waarom zou ik me zorgen maken over federatie in mijn instant messaging app? {#faq-users-federation}

De Jabber/XMPP-federatie is een gedecentraliseerd netwerk van servers die met elkaar communiceren. Dit betekent dat er geen centraal punt van controle of falen is, waardoor het netwerk zeer veerkrachtig is. Aangezien er geen [silo](https://en.wikipedia.org/wiki/Information_silo) is die je gegevens en contacten gijzelt, wordt [vendor lock-in](https://nl.wikipedia.org/wiki/Vendor_lock-in) onmogelijk. Iedereen kan zijn eigen server opzetten en zich bij het netwerk aansluiten, en vanaf elke account kun je onmiddellijk communiceren met miljoenen gebruikers over de hele wereld.

### Waarom is er zo'n verwarrende verscheidenheid aan diensten en toepassingen? {#faq-user-variety}

Net zoals e-mail, wordt Jabber/XMPP niet beheerd door één enkel bedrijf, dus is er een verscheidenheid aan diensten en clients, elk gericht op verschillende gebruikssituaties. JoinJabber is opgericht om het eenvoudiger te maken de dienst en client te vinden die het beste bij jouw gebruikssituatie past. Als een onafhankelijke door vrijwilligers gerunde gemeenschap doen we onze aanbevelingen zonder enige ongepaste invloed van commerciële verkopers. Als onze website/documentatie je (nog) niet helpt, moedigen we je aan om opbouwende kritiek te geven zodat we het kunnen verbeteren.

### Biedt JoinJabber Jabber/XMPP accounts aan? Waar kan ik me hiervoor aanmelden? {#faq-users-accounts}

JoinJabber biedt geen gebruikersaccounts aan. Onze Jabber/XMPP-server wordt alleen gebruikt voor het hosten van gerelateerde groepschats en voor het overbruggen van chatrooms naar andere netwerken, zoals IRC en Matrix. We hebben al vroeg besloten dat we geen onvervangbaar onderdeel van het ecosysteem willen worden, maar liever reclame maken voor andere bestaande oplossingen.

### Worden mijn gegevens verkocht aan adverteerders? Ben ik het product? {#faq-users-privacy}

De Jabber/XMPP gemeenschap wordt over het algemeen gerund door enthousiaste vrijwilligers zonder winstoogmerk, ook al zijn er bedrijven die er inmiddels een zakelijk belang in hebben. De software die wij aanbevelen is vrije en open-source software met transparante financieringsbronnen. Specifieke servers kunnen inbreuk maken op je privacy, maar dit is geen gangbare praktijk in het hele Jabber/XMPP-ecosysteem en we doen ons best om er zeker van te zijn dat dienstverleners die geadverteerd worden op [JoinJabber](https://joinjabber.org/) te goeder trouw zijn.

Bovendien kunnen sommige dienstverleners handige functies aanbieden die je misschien wel of niet leuk vindt. Bijvoorbeeld, [quicksy.im](https://quicksy.im/) is expliciet ontworpen om gebruikers zich te laten registreren met hun telefoonnummer, zoals ze doen op Whatsapp of Signal. Als dat je niet bevalt, ben je vrij om een andere dienstverlener te gebruiken, en met je vrienden te communiceren op Quicksy. Tenslotte, als jij of je gezelschap een eigen Jabber/XMPP-server draait, heb jij de touwtjes in handen en mag jij bepalen wat er met de gegevens gebeurt.

### Wordt het XMPP of Jabber genoemd? Is er een verschil? {#faq-users-xmppjabber}

Dit is een controversieel onderwerp, maar er is in principe geen verschil, behalve de naam die men verkiest te gebruiken. Traditioneel verwijst XMPP naar het protocol, terwijl Jabber verwijst naar de open federatie van servers die het XMPP-protocol hanteren. Beide kunnen echter door elkaar worden gebruikt.

"Jabber" in de omgangstaal mag niet worden verward met het handelsmerk "Jabber", dat sinds ten minste 2008 [eigendom](https://www.cisco.com/c/en/us/about/legal/trademarks.html) is van Cisco Systems, Inc. Het handelsmerk is verwant met XMPP in die zin dat de verschillende producten van Cisco met de naam Jabber gebaseerd zijn op XMPP, zij het dat ze over het algemeen niet compatibel zijn met het open XMPP/Jabber-ecosysteem.

### Ondersteunt XMPP/Jabber encryptie? Zijn mijn gesprekken veilig? {#faq-users-encryption}

Er zijn twee soorten encryptie die wij ondersteunen en promoten:

- transportbeveiliging (netwerkencryptie)
- end-to-endversleuteling (inhoudsversleuteling)

Verbindingen van/naar/tussen servers zijn meestal beveiligd met [TLS](https://nl.wikipedia.org/wiki/Transport_Layer_Security), wat niet 100% veilig is, maar gebruikers en servers wel beschermt tegen typische aanvallers op het netwerk. Alternatieve transportmechanismen voor het XMPP-protocol, zoals de Onion-diensten van Tor, kunnen betere/aanvullende beveiliging bieden.

End-to-endversleuteling wordt ondersteund door verschillende extensies, met name de OMEMO-encryptie (afgeleid van het Signal-protocol) en PGP-versleuteling. Veel XMPP-clients gebruiken OMEMO al standaard en maken het zo gemakkelijk om XMPP te gebruiken zonder je privacy in gevaar te brengen. Deze implementaties zijn echter niet onderworpen aan een beveiligingsaudit. Als een beveiligingsaudit voor jou een vereiste is, zie dan de [Security FAQ](@/faqs/security/_index.nl.md).

### Ik heb gelezen dat XMPP/Jabber vervangen is door het nieuwere Matrix protocol {#faq-users-matrix}

XMPP en Matrix zijn twee ongerelateerde protocollen met vergelijkbare doelen en eigenschappen.  Beide zijn [gefedereerd]( #faq-users-federation). Geen van beide vervangt de ander en verbeterde interoperabiliteit is een doel aan beide kanten. XMPP bestaat al erg lang en is de officiële IETF-standaard voor real-time berichtenverkeer, waaraan iedereen kan bijdragen en de specificaties kan uitbreiden. Als je niet zeker weet of je Matrix of XMPP (of beide) moet gebruiken, kun je een (ietwat bevooroordeelde) [vergelijking](@/faqs/advanced/_index.nl.md) vinden van de sterke punten van beide protocollen.

### Kan ik een account registreren met mijn telefoonnummer, zodat mijn contacten mij gemakkelijk kunnen vinden? {#faq-users-phone}

Er is een Jabber/XMPP-dienst genaamd [Quicksy](https://quicksy.im/) die precies dat doet. Dit wordt echter meestal beschouwd als een anti-privacyfunctie, omdat telefoonnummers in de meeste landen gemakkelijk gekoppeld kunnen worden aan overheids-ID's. Bovendien zijn mobiele telefoons een middel voor toezicht en daadwerkelijke aanvallen op de gebruikers (zoals [Facebook](https://www.independent.co.uk/life-style/gadgets-and-tech/news/facebook-phone-numbers-data-breach-privacy-a9092641.html), bij [meer dan één gelegenheid](https://appleinsider.com/articles/19/03/04/facebook-embroiled-in-yet-another-privacy-scandal-this-time-involving-your-phone-number)). Wij zijn echter van mening dat het volledig in orde is dat mensen vrijwillig hun telefoonnummer weggeven, zolang de dienst [geen inbreuk maakt op de privacy van je vrienden](https://www.huffpost.com/entry/facebook-phonebook-contacts_n_924543).

### Kan ik audio-/videogesprekken of conferenties voeren met XMPP/Jabber? {#faq-users-audiovideo}

Het korte antwoord is **ja**. Een decennium geleden waren er wat onhandige pogingen tot audio/video conferencing, maar in de loop der jaren is het systeem verfijnd. De nieuwe op de WebRTC-standaard gebaseerde specificaties daarvoor zijn solide, hoewel ze nog niet op grote schaal in clients zijn geïmplementeerd. Video-conferencing vereist aanzienlijk meer serverresources en wordt niet op grote schaal ondersteund op alle servers. [Jitsi Meet](https://jitsi.org/jitsi-meet/) is een zeer populaire gratis software-oplossing, gebaseerd op XMPP, die kan worden ingezet voor video-conferencing. In feite is dit de oplossing die ook door de [matrixgemeenschap](https://matrix.org/blog/2021/01/04/taking-fosdem-online-via-matrix) wordt gebruikt.

### Als XMPP/Jabber zo geweldig is, waarom is het dan niet populairder? {#faq-users-popularity}

De meeste mensen gebruiken XMPP-technologie elke dag zonder het te beseffen! XMPP vormt de basis van veel commerciële platforms (zoals WhatsApp en Zoom) en wordt veel gebruikt in de gamingindustrie voor multiplayer-toepassingen. Veel grote bedrijven en organisaties gebruiken het ook voor hun interne communicatie. Jammer genoeg zijn weinig van deze toepassingen open voor de bredere XMPP-federatie. Sommige toepassingen die in het verleden open waren, hebben besloten om deze interoperabiliteit te sluiten, hoofdzakelijk om op korte termijn winst te kunnen maken.

Het non-profit Jabber/XMPP-ecosysteem groeit elke dag, maar tot voor kort was er geen gemeenschaps om non-profit gebruikssituaties te promoten. Daarom zijn we het JoinJabber-collectief gestart: up-to-date informatie bieden voor nieuwkomers in het Jabber/XMPP ecosysteem.
