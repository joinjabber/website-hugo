---
title: "FAQ voor gevorderden"
---

Geavanceerde vragen voor/van eindgebruikers.

### Hoe duurzaam is XMPP/Jabber? {#faq-advanced-sustainability}

Zal mijn account over 10 jaar nog werken?

XMPP/Jabber werd gestandaardiseerd in het begin van de jaren 2000 (20 jaar geleden) en sommige diensten zijn slechts een paar jaar jonger. Waarschijnlijk zullen ze over tien jaar nog steeds operationeel zijn. Andere servers worden uitgebaat door non-profits met een duurzaam model: verenigingen die vrije software en privacy promoten (zoals [April](https://chapril.org/), [5July](https://5july.org/) en [La Quadrature](https://www.laquadrature.net/en/)), non-profit Internet Service Providers (zoals [franciliens.net](https://www.franciliens.net/) en [ARN](https://arn-fai.net/)), verenigingen gewijd aan Jabber/XMPP (zoals [jabber.fr](https://jabber.fr/) en vele anderen), of hosting-coöperatieven (zoals [disroot](https://disroot.org/)). Deze servers hebben goede kansen om over tien jaar nog steeds te functioneren.

Andere servers, gerund door bedrijven, universiteiten en enthousiastelingen, kunnen over tien jaar nog steeds draaien, omdat het hosten van Jabber/XMPP-diensten zeer weinig middelen vereist. Er is echter geen manier om dat zeker te weten, omdat hun verdienmodel en drijfveren misschien niet op één lijn liggen met die van hun gebruikers. In het Jabber/XMPP-ecosysteem is het echter gebruikelijk dat een beheerder die zijn activiteiten staakt, zijn gebruikers lang van tevoren waarschuwt (omdat er geen zakelijke reden is om dat niet te doen), zodat ze tijd hebben om naar een andere server te migreren.

Het hosten van Jabber/XMPP-diensten is niet ingewikkeld voor systeembeheerders, dus de meest toekomstbestendige optie is wellicht het draaien van een eigen server. Als je lid bent van een non-profitvereniging, een coöperatie, of een andere collectieve entiteit met een speciaal systeembeheerdersteam, overweeg dan om je eigen server te draaien. Als je diensten al worden beheerd door een derde partij en je hebt er vertrouwen in dat ze nog lang zullen blijven bestaan, dan kun je hen vragen om een Jabber/XMPP-dienst voor je te hosten, naast het gebruikelijke web/e-mail/DNS-aanbod.

### Jabber/XMPP heeft een bepaalde feature niet, waarom gaat het zo traag? {#faq-advanced-evolution}

XMPP is ontworpen om gemakkelijk uitbreidbaar te zijn en is de laatste jaren geëvolueerd tot een modern ecosysteem voor real-time berichtenuitwisseling. Als je slechte herinneringen hebt van een decennium geleden, probeer het dan nu nog een keer uit: sommige oudere clients hebben niet alle functies die je zou verwachten van een moderne messenger, maar de via deze site beschikbare zouden echt moeten voldoen. Laat het ons weten als dat niet het geval is.

Omdat XMPP een standaardprotocol is, worden alle voorstellen publiekelijk beoordeeld en ontwikkeld, meestal via mailinglijsten en op de website van de [XMPP Standards Foundation](https://xmpp.org/). Dit klinkt misschien alsof het innovatie verhindert, maar in feite is het tegendeel waar. Het specificatieproces, hoe omslachtig het ook is, weerhoudt ontwikkelaars er niet van bestaande functies uit te breiden of nieuwe functies te implementeren omdat een nieuwe specificatie (nog) niet is goedgekeurd. Het helpt alleen andere ontwikkelaars om mee te gaan in de ontwikkeling als en wanneer zij dat willen.

### Hoe verhoudt XMPP zich tot Matrix in meer detail? {#faq-advanced-matrix}

_Een deels subjectieve vergelijking (voel je vrij om het hier niet mee eens te zijn)_

**Jabber/XMPP:**
- gerenommeerd en stabiel
- Veel clients op de meeste platformen, sommige met een moderne set mogelijkheden, andere wat minder
- een toegewijd, voornamelijk non-profit, ecosysteem, zowel voor clients als servers
- schaalbaar tot veel gebruikers met weinig middelen aan de server-kant (denk *Raspberry Pi*)
- werkt goed met Tor en andere privacy-vriendelijke proxies
- chatrooms en servers minimaliseren het delen van meta-data, waardoor het meer je privacy beschermt, vooral als je je eigen XMPP-server draait
- hoewel bridging naar andere IM-systemen beschikbaar is, zijn de inspanningen de laatste jaren teruggeschroefd
- heeft in de loop der jaren een zekere "technische schuld" opgebouwd

**Matrix:**
- Element (de officiële web-based Matrix-client), heeft een moderne interface en gebruikerservaring
- chatrooms zijn censuurbestendig, omdat ze niet op één enkele server staan
- toegewijde financiering/middelen om het ecosysteem sneller vooruit te helpen
- voornamelijk ontwikkeld door een bedrijf met winstoogmerk in het VK
- een referentie-implementatie (Synapse) die veel serverresources vergt, waardoor het moeilijk/kostbaar is om deze zelf te hosten
- een relatief gecentraliseerd federaal netwerk, met (standaard) gecentraliseerde identiteitsdiensten
- een inzet voor [peer-to-peer gebruikssituaties](https://matrix.org/blog/2020/06/02/introducing-p-2-p-matrix/) (minder geavanceerd [in het XMPP ecosysteem](https://xmpp.org/extensions/xep-0174.html))
- nadruk op overbrugging naar andere IM-systemen met gateways die door de hoofdontwikkelaars zijn gemaakt
- heeft een nog vrij nieuwe en voor een groot deel experimentele code-base

Zoals je ziet, hebben beide protocollen hun sterke en zwakke punten, maar ze werken nog steeds volgens vergelijkbare principes. In theorie zou het mogelijk (en gemakkelijker) zijn geweest om het matrix protocol bovenop XMPP te bouwen om [fragmentatie van het ecosysteem](https://xkcd.com/927/) te vermijden. Waarom dit niet is gebeurd is een goede vraag, maar feit is dat we nu twee protocollen/ecosystemen hebben met vergelijkbare doelen, en dat we moeten proberen onze ego's te temperen en te werken aan interoperabiliteit, ten behoeve van de eindgebruikers.
