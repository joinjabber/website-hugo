---
title: "Los geht's"
weight: 1
---

Einfach in zwei Schritten:

1. Melde dich auf einem Server an

{{< button "servers/" "Finde einen Server" "mb-1" >}}

2. Suche dir eine App aus

{{< button "apps/" "Wähle eine App aus" >}}

Glückwunsch! Jetzt kannst du due von dir gewählte App öffnen, deine Zugangsdaten eingeben, und dann anfangen mit deinen Freunden zu chatten.

{{< tip "warning" >}}
Jabberadressen (abgekürzt zu JID) schauen genau wie E-mail Adressen aus. Erstellst du zum Beispiel einen Zugang wie `emma.goldman` auf dem Server `example.org`, so wäre deine Adresse `emma.goldman@example.org`.

Gruppenchats haben eine ähnliche Adresse wie zum Beispiel `chat@groups.example.org`.
{{< /tip >}}

Noch Niemanden zum chatten? Du kannst auch bei unserem Gruppenchat vorbeischauen: {{< chatlink "chat" >}}chat@joinjabber.org (web chat){{< /chatlink >}}.

Wir haben auch einen beginnerfreundlichen Supportchat: {{< chatlink "support" >}}support@joinjabber.org (web chat){{< /chatlink >}}. Der Webchat funktioniert ganz ohne Zugangsdaten!

Weitere Fragen? Antworten gibt es hier:
{{< button "faqs/" "FAQ" "mb-1" >}}
