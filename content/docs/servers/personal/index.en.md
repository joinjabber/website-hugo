---
title: "Personal"
description: "personal servers"
weight: 3
---

You may be looking for a non-profit hosting provider with a sustainable economic model, that has more chances of being up and running 20 years from now. Or you rather need a server that offers anonymous access via the Tor network? Here are some recommended options:

## Chalec

[<img alt="Create an account" src="/images/servers/chalec.png" style="max-height:100px;height:100%">](https://www.chalec.org/services/xmpp.html)

[Chalec.org](https://www.chalec.org/services/xmpp.html) is a non-commercial provider from France. You can sign up for their service directly from within [your app](/docs/apps/).

## XMPP.social

[<img alt="Create an account" src="/images/servers/hookipa.png" style="max-height:60px;height:100%">](https://hookipa.net/register/new)

XMPP.social is one of several domains you can register for free with [Hookipa.net](https://hookipa.net/). Their servers are located in Germany.

## Disroot

[<img alt="Create an account" src="/images/servers/disroot.svg" style="max-height:40px;height:100%">](https://user.disroot.org/pwm/public/newuser)

Disroot is a non-commercial project based in the Netherlands and founded in 2015. It is run by a group of volunteers and offers various online services. You can learn more about them [on their website here](https://disroot.org/en/about).

## XMPP.is

[<img alt="Create an account" src="/images/servers/xmppis.png" style="max-height:60px;height:100%">](https://xmpp.is/account/register/)

[XMPP.is](https://xmpp.is/) is a privacy focused provider with servers located in Iceland. They offer [sign ups and direct access via the Tor network](https://xmpp.is/2021/11/10/tor-hsv3-registrations-are-now-open/).

**Others?**

This is obviously not an exhaustive list and if you have a really good recommendation please contact us here: {{< chatlink "chat" >}}chat@joinjabber.org (web chat){{< /chatlink >}}

If you are the server operator of one of these services and disagree with being listed here, please contact us: {{< chatlink "servers" >}}servers@joinjabber.org (web chat){{< /chatlink >}}.

