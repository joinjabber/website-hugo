---
title: "Collectief"
description: "Servers voor een collectief"
weight: 2
---

Hier vind je enkele aanbevolen Jabber/XMPP-dienstverleners die een XMPP-server aanbieden voor collectief gebruik door een groep of organisatie. Het collectief kan de eigen domeinnaam gebruiken voor Jabber ID's, bijvoorbeeld `naam@organisatie.org`.

## Conversations.im

[<img alt="Conversations domain accounts" src="/images/servers/conversations.svg" style="max-height:60px;height:100%"><span style="color:var(--text);font-weight:bold">&nbsp;&nbsp;Conversations.im</span>](https://account.conversations.im/domain/)

Conversations.im is het commerciële aanbod van de ontwikkelaar van de app [Conversations]({{< ref "docs/apps/android#conversations-android" >}}). Lees meer over hoe je een eigen domeinnaam kunt registreren en koppelen op hun [website](https://account.conversations.im/domain/). De diensten worden gehost in Duitsland.

## Hot-Chilli

[<img alt="Hot-Chilli Jabber hosting" src="/images/servers/hotchilli.png" style="max-height:60px;height:100%">](https://jabber.hot-chilli.net/jabber-hosting/)

Hot-Chilli wordt gerund door een IT-bedrijf uit Duitsland en heeft sinds 2005 een gratis XMPP-server. Lees meer informatie over de dienst op [hun website](https://jabber.hot-chilli.net/). Het is mogelijk om [een eigen domeinnaam te verbinden met hun XMPP-server](https://jabber.hot-chilli.net/how-tos/dns-settings-for-jabber-hosting/). 

## Snikket

[<img alt="Snikket hosting" src="/images/servers/snikket.svg" style="max-height:60px;height:100%">](https://snikket.org/hosting/)

Snikket is een project gestart door een van de [Prosody](https://prosody.im)-ontwikkelaars met als doel een uniforme ervaring en een [gebruiksvriendelijke serverhosting](https://snikket.org/hosting/) te bieden. Je kunt meer informatie vinden op [hun website](https://snikket.org/about/goals/).

## Tigase

[<img alt="Tigase pricing plans" src="/images/servers/tigase.png" style="max-height:60px;height:100%">](https://tigase.net/pricing-xmpp/)

De ontwikkelaars van de Tigase XMPP-server bieden ook professionele server hosting aan voor grote organisaties.

**Meer servers?**

Dit is uiteraard geen volledige lijst! Als je een echt goede aanbeveling hebt, neem dan [contact met ons op](xmpp:chat@joinjabber.org?join). 

Als je beheerder bent van een van deze diensten en het er niet mee eens bent om hier vermeld te worden, neem dan ook [contact met ons op](xmpp:servers@joinjabber.org?join).
