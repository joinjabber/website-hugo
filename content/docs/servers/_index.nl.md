---
title: "Servers"
description: "Aanbevolen servers"
weight: 2
---

Eerst moet je bepalen welke van de volgende categorieën het best beschrijft wat je zoekt.

### I want an easy all-inclusive start:

{{< button "all-in-one/" "All-In-One" >}}

_This category is for people new to XMPP that are looking for an option that combines everything in an easy package._

### Ik ben geïnteresseerd in een dienst voor mijn:

{{< button "collective/" "Collectief" >}}

_Deze categorie is voor groepen en organisaties die meerdere accounts en professionele hosting nodig hebben._

### Ik wil graag een:

{{< button "personal/" "Persoonlijk account" >}}

_Deze categorie is voor personen die op zoek zijn naar een betrouwbare plaats om een account te creëren._

### Waarom zijn er zoveel opties om uit te kiezen?

Het Jabber-netwerk is een grote en gedecentraliseerde inspanning van vele mensen en organisaties. Er is geen centraal bedrijf dat de lakens uitdeelt en de protocolontwikkeling is collectief georganiseerd via de [XMPP Standards Foundation](https://xmpp.org/about/xmpp-standards-foundation/). Dit betekent dat er veel servers zijn om uit te kiezen.

### Accounts migreren

Ben je van gedachten veranderd of heb je een fout gemaakt bij het selecteren van een server? Geen probleem! Hoewel accountportabiliteit nog steeds [in ontwikkeling](https://docs.modernxmpp.org/projects/portability/) is voor het grootste deel van het Jabber netwerk, is er al een website die het [grotendeels automatiseert](https://migrate.modernxmpp.org/). Aangezien het momenteel je inloggegevens vereist om te functioneren, is het waarschijnlijk goed om de wachtwoorden van beide accounts te wijzigen na gebruik van deze tool.

### Kan ik niet gewoon een account bij jullie krijgen?

Sorry, maar wij verstrekken zelf geen Jabber-accounts. Je kunt [hier]({{< ref "about" >}}) meer te weten komen over de redenen waarom.
