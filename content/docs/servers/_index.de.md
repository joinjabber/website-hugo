---
title: "Servers"
description: "Empfohlende servers"
weight: 2
---

Als Erstes musst du überlegen welche der folgenden Kategorien am besten beschreiben welche Art von Jabberserver du benötigst.

### Ich hätte gerne ein einfaches Komplettpaket:

{{< button "all-in-one/" "All-In-One" >}}

_Diese Kategorie ist für Leute die neu bei XMPP sind und nach einer einfachen Option suchen die schon alles beinhaltet._

### Ich habe interesse an einem Server für mein:

{{< button "collective/" "Kollektiv" >}}

_Diese Kategorie ist für Gruppen und Organisationen die mehrere Jabberaddressen und eine professionelle Dienstleistung benötigen._

### Ich hätte gerne einen:

{{< button "personal/" "Persönlichen Zugang" >}}

_Diese Kategorie ist für Individuen die einen verlässlichen Jabberserver haben wollen._

### Warum gibt es so viel Auswahl?

Das Jabbernetzwerk ist eine weitreichendes und dezentralisiertes Projekt an dem viele Individuen und Organisationen teilnehmen. Es gibt keine zentrale Firma die alles bestimmt und die Protokollentwicklung ist gemeinschaftlich durch die [XMPP Standards Foundation](https://xmpp.org/about/xmpp-standards-foundation/) organisiert. Dies bedeutet, dass es viele Server zur Auswahl gibt.

### Serverumzug

Du hast dich umentschieden oder einen Fehler bei der ursprünglichen Serverwahl gemacht? Kein großes Problem! Einfacher Serverumzug ist zwar noch im [Aufbau](https://docs.modernxmpp.org/projects/portability/) für große Teile des Jabbernetzwerkes, aber es gibt [hier](https://migrate.modernxmpp.org/) eine Webseite die den Umzug erleichtert. Da diese Webseite deine Zugangsdaten erfordert, ist es empfehlenswert die Passworter nach Benutzung zu ändern.

### Kann ich nicht einfach einen Zugang hier bekommen?

Nein, wir von JoinJabber bieten keine Jabberzugänge an. Genaueres hierzu kannst du in den Zielen unseres Projektes [hier](/about) finden.
