---
title: "Get started"
weight: 1
aliases:
    - "/get-started/"
---

There are only two easy steps:

1. Register an account on a server

{{< button "servers/" "Find a server" "mb-1" >}}

2. Pick a client app

{{< button "apps/" "Pick an app" >}}

Congratulations! You can now open your client and give it your Jabber address to connect to the server and start chatting with some friends.

{{< tip "warning" >}}
Jabber addresses (also called JIDs) look just like email addresses. So if you create the account `emma.goldman` on server `example.org`, then your address will be `emma.goldman@example.org`.

Group chats have similar addresses, like `chat@groups.example.org`.
{{< /tip >}}

Feeling lonely? You can join us in the community chat: {{< chatlink "chat" >}}chat@joinjabber.org (web chat){{< /chatlink >}}.

We also have a friendly support chat: {{< chatlink "support" >}}support@joinjabber.org (web chat){{< /chatlink >}}. You can join the chat without registering an account!

If you want to create an invite link for sharing with someone else, feel free to use our [easy to use tool](https://invite.joinjabber.org/) for that.

More questions? Read our Frequently Asked Questions:
{{< button "faqs/" "FAQ" "mb-1" >}}
