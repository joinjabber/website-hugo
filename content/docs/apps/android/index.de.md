---
title: "Android"
---

## [<img src="/images/apps/conversations.svg" style="max-height:30px;height:100%"> Conversations](#conversations-android) {#conversations-android}

Wir empfehlen [Conversations](https://conversations.im). Du kannst diese App auf [F-Droid](https://f-droid.org/en/packages/eu.siacs.conversations/), den freien App-Store für Android herunterladen. Falls du F-Droid noch nicht installiert hast, empfehlen wir es wärmsten sich ein wenig Zeit dafür zu nehmen. Falls du die Entwicklung von Conversations unterstützen möchtest, kannst du die App auch im Google Play Store kaufen oder an das Projekt [spenden](https://conversations.im/#donate). Hier geht's zum Download:

[<img alt="Get it on F-Droid" src="/images/apps/fdroid.png" style="max-height:65px;height:100%">](https://f-droid.org/en/packages/eu.siacs.conversations/)
[<img alt="Get it on Google Play" src="/images/apps/google.png" style="max-height:65px;height:100%">](https://play.google.com/store/apps/details?id=eu.siacs.conversations)

## [<img src="/images/apps/cheogram.svg" style="max-height:30px;height:100%"> Cheogram](#cheogram-android) {#cheogram-android}

[Cheogram](https://cheogram.com/) basiert auf Conversations aber beinhaltet einige weitere Funktionen. Hier geht's zum Download:

[<img alt="Get it on F-Droid" src="/images/apps/fdroid.png" style="max-height:65px;height:100%">](https://f-droid.org/en/packages/com.cheogram.android/)
[<img alt="Get it on Google Play" src="/images/apps/google.png" style="max-height:65px;height:100%">](https://play.google.com/store/apps/details?id=com.cheogram.android.playstore)

## [<img src="/images/apps/monocles.svg" style="max-height:30px;height:100%"> Monocles Chat](#monocles-android) {#monocles-android}

[Monocles chat](https://monocles.de/more/) ist ein weiterer Fork von Conversations mit einem Schwerpunkt auf Benutzerfreundlichkeit. Lade die App auf F-Droid herunter or spende and den Entwickler durch einen Kauf auf dem Playstore:

[<img alt="Get it on F-Droid" src="/images/apps/fdroid.png" style="max-height:65px;height:100%">](https://f-droid.org/en/packages/de.monocles.chat/)
[<img alt="Get it on Google Play" src="/images/apps/google.png" style="max-height:65px;height:100%">](https://play.google.com/store/apps/details?id=eu.monocles.chat)

## [<img src="/images/apps/movim.svg" style="max-height:30px;height:100%"> Movim](#movim-android) {#movim-android}

[Movim](https://movim.eu) ist eine Webanwendung die gut in mobilen Webbrowsern funktioniert. Sie kann auch als [PWA app](https://en.wikipedia.org/wiki/Progressive_web_app) installiert werden.
