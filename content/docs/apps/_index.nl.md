---
title: "Apps"
description: "Apps voor je besturingssysteem"
weight: 3
---

Omdat Jabber een open netwerk is, zijn er veel client-apps om uit te kiezen. Maar om je keuze wat makkelijker te maken, hebben we geprobeerd de beste apps voor je te selecteren.

Eerst moet je beslissen waar je de app installeert:

### Voor mobiele telefoons en tablets:

{{< button "android/" "Android" "mb-1" >}}

{{< button "ios/" "iOS" >}}

_Voor andere mobiele platforms hebben we op dit moment helaas nog geen goede aanbevelingen._

### Voor desktops en laptops:

{{< button "gnulinux/" "GNU/Linux" "mb-1" >}}

{{< button "macos/" "Mac OS" >}}

{{< button "windows/" "Windows" >}}

_Er zijn ook platformonafhankelijke webclients zoals [Movim](https://join.movim.eu)._
