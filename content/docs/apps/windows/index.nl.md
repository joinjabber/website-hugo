---
title: "Windows"
---

## [<img src="/images/apps/gajim.svg" style="max-height:30px;height:100%"> Gajim](#gajim-windows) {#gajim-windows}

[Gajim](https://gajim.org/) is een volledig uitgeruste Jabber/XMPP-applicatie voor o.a. Windows. Installeer het via [de website](https://gajim.org/download/) of hier:

[<img alt="Get it from Microsoft" src="/images/apps/microsoft.svg" style="min-height:65px;height:100%">](https://apps.microsoft.com/store/detail/gajim/9PGGF6HD43F9)

## [<img src="/images/apps/movim.svg" style="max-height:30px;height:100%"> Movim](#movim-windows) {#movim-windows}

[Movim](https://movim.eu) is een web-client die goed werkt in mobiele browsers, zoals Firefox of Chromium. Het is te installeren als een [Progressive Web App](https://en.wikipedia.org/wiki/Progressive_web_app) door eerst een [Movim-instantie](https://join.movim.eu) te openen en vervolgens in je browsermenu de optie "Toevoegen aan beginscreen" te kiezen. Pushmeldingen kunnen worden ingeschakeld in de instellingen van Movim: meldingen zullen via de web-pushfunctie van de browser worden afgeleverd.
