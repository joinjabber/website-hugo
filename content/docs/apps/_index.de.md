---
title: "Apps"
description: "Apps für dein Betriebssystem"
weight: 3
---

Da Jabber ein offenes Netzwerk ist, gibt es viele verschiedene Apps zur Auswahl. Aber um es dir etwas zu erleichtern, haben wir versucht die besten Apps auszuwählen. 

Als Erstes musst du auswählen wo du die App installieren willst: 

### Für Handys und Tablets:

{{< button "android/" "Android" "mb-1" >}}

{{< button "ios/" "iOS" >}}

_Leider haben wir für andere mobile Platformen derzeit keine Empfehlungen._

### Für Desktop- und Laptopcomputer:

{{< button "gnulinux/" "GNU/Linux" "mb-1" >}}

{{< button "macos/" "Mac OS" >}}

{{< button "windows/" "Windows" >}}

_Es gibt auch platformunabhängige Apps die in Webbrowser laufen, z.B. [Movim](https://join.movim.eu)._
