---
title: "GNU/Linux"
---

## [<img src="/images/apps/dino.svg" style="max-height:30px;height:100%"> Dino](#dino-linux) {#dino-linux}

[Dino](https://dino.im/) is een nieuwe, modern vormgegeven Jabber/XMPP-applicatie voor GNU/Linux. Het ondersteunt audio- en videobellen. Installeer het via de pakketbeheerder van je distributie of via:

[<img alt="Get it on Flathub" src="/images/apps/flathub.svg" style="max-height:65px;height:100%">](https://flathub.org/apps/im.dino.Dino)

## [<img src="/images/apps/gajim.svg" style="max-height:30px;height:100%"> Gajim](#gajim-linux) {#gajim-linux}

[Gajim](https://gajim.org/) is een volledig uitgeruste Jabber/XMPP-applicatie die draait op o.a. GNU/Linux. Installeer het via de pakketbeheerder van je distributie of via:

[<img alt="Get it on Flathub" src="/images/apps/flathub.svg" style="max-height:65px;height:100%">](https://flathub.org/apps/org.gajim.Gajim)
