---
title: "iOS"
---

## [<img src="/images/apps/monal.svg" style="max-height:30px;height:100%"> Monal](#monal-ios) {#monal-ios}

[Monal](https://monal-im.org/) is a modern Jabber/XMPP client for iOS and Mac OS that works well with most XMPP servers. Get it here:

[<img alt="Download on the App Store" src="/images/apps/apple.png" style="max-height:65px;height:100%">](https://apps.apple.com/app/id317711500)

## [<img src="/images/apps/siskin.png" style="max-height:30px;height:100%"> Siskin](#siskin-ios) {#siskin-ios}

[Siskin](https://siskin.im/) is another Jabber/XMPP client for iOS. It supports audio and video calls. Get it here:

[<img alt="Download on the App Store" src="/images/apps/apple.png" style="max-height:65px;height:100%">](https://apps.apple.com/app/us-app-tigase-messenger/id1153516838)

*Note*: Reliable push notifications with the Siskin client requires the use of a [Tigrase](https://tigase.net/xmpp-server/) or [Snikket](https://snikket.org/) XMPP server.

## [<img src="/images/apps/movim.svg" style="max-height:30px;height:100%"> Movim](#movim-ios) {#movim-ios}

[Movim](https://movim.eu) is a web-client that works well in the Safari mobile browser. It can be installed as a [PWA app](https://en.wikipedia.org/wiki/Progressive_web_app) by visiting a [Movim instance](https://join.movim.eu) and selecting the "Add to home screen" option from the menu of your browser. Push notifications can be enabled in the Movim settings and will be delivered through the browser's web-push feature.

