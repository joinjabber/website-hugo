---
title: "Mac OS"
---

## [<img src="/images/apps/monal.svg" style="max-height:30px;height:100%"> Monal](#monal-macos) {#monal-macos}

[Monal](https://monal-im.org/) ist eine moderne Jabber/XMPP App für iOS und Mac OS. Hier geht's zum Download für Mac OS:

[<img alt="Download on the App Store" src="/images/apps/apple.png" style="max-height:65px;height:100%">](https://apps.apple.com/app/id1637078500)

## [<img src="/images/apps/beagle.png" style="max-height:30px;height:100%"> Beagle](#beagle-macos) {#beagle-macos}

[Beagle](https://beagle.im/) ist eine Jabber/XMPP App for Mac OS. Hier geht's zum Download:

[<img alt="Download on the App Store" src="/images/apps/apple.png" style="max-height:65px;height:100%">](https://apps.apple.com/us/app/beagleim-by-tigase-inc/id1445349494)
