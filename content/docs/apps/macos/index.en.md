---
title: "Mac OS"
---

## [<img src="/images/apps/monal.svg" style="max-height:30px;height:100%"> Monal](#monal-macos) {#monal-macos}

[Monal](https://monal-im.org/) is a modern Jabber/XMPP client for iOS and Mac OS. Get it here:

[<img alt="Download on the App Store" src="/images/apps/apple.png" style="max-height:65px;height:100%">](https://apps.apple.com/app/id1637078500)

## [<img src="/images/apps/beagle.png" style="max-height:30px;height:100%"> Beagle](#beagle-macos) {#beagle-macos}

[Beagle](https://beagle.im/) is a Jabber/XMPP client for Mac OS. Get it here:

[<img alt="Download on the App Store" src="/images/apps/apple.png" style="max-height:65px;height:100%">](https://apps.apple.com/us/app/beagleim-by-tigase-inc/id1445349494)

## [<img src="/images/apps/movim.svg" style="max-height:30px;height:100%"> Movim](#movim-macos) {#movim-macos}

[Movim](https://movim.eu) is a web-client that works well in the Safari browser. It can be installed as a [PWA app](https://en.wikipedia.org/wiki/Progressive_web_app) by visiting a [Movim instance](https://join.movim.eu) and selecting the "Add to home screen" option from the menu of your browser. Push notifications can be enabled in the Movim settings and will be delivered through the browser's web-push feature.
